# git-push-conflict

Trying to mess up with mixed commits from various users to learn about git rebase

## Setup

### Create the source repo

Create a folder to house this project. In this example, we'll call it `~/git-push-conflict`

```
~ $ mkdir ~/git-push-conflict
```

Create a `source` folder inside that one

```
~ $ mkdir ~/git-push-conflict/source
```

Go to the source folder and initialize a bare repo

```
~ $ cd ~/git-push-conflict/source
```
```
~/git-push-conflict $ git init --bare
```

### Make a couple clones with different users

Go to the `git-push-conflict` folder

```
~ $ cd ~/git-push-conflict
```

```
~/git-push-conflict $ git clone $(pwd)/source user1
```

Get inside the new clone of the repo
```
~/git-push-conflict $ cd user1
```

Configure a fake user
```
~/git-push-conflict/user1 $ git config --local user.name "user 1"
~/git-push-conflict/user1 $ git config --local user.email "user1@test.net"
```

Go back to the `git-push-conflict` folder and repeat for `user2`

## Initial empty commit

### User 1

```
~/git-push-conflict/user1 $ git commit --allow-empty -m "Initial (empty) commit"
~/git-push-conflict/user1 $ git tag initial
~/git-push-conflict/user1 $ git push -u origin --all
~/git-push-conflict/user1 $ git push origin --tags
```

### User 2

```
~/git-push-conflict/user2 $ git pull
```

## First conflict

### User 1

```
~/git-push-conflict/user1 $ echo "# git-push-conflict" > README.md
~/git-push-conflict/user1 $ git add README.md
~/git-push-conflict/user1 $ git commit -m "Add README"
~/git-push-conflict/user1 $ git push
```

### User 2

```
~/git-push-conflict/user2 $ echo "# Ignore some things" > .gitignore
~/git-push-conflict/user2 $ git add .gitignore
~/git-push-conflict/user2 $ git commit -m "Add .gitignore"
~/git-push-conflict/user2 $ git push
```

Here comes the fun
```
To /home/user/git-push-conflict/source
 ! [rejected]        master -> master (fetch first)
error: failed to push some refs to '/home/user/git-push-conflict/source'
hint: Updates were rejected because the remote contains work that you do
hint: not have locally. This is usually caused by another repository pushing
hint: to the same ref. You may want to first integrate the remote changes
hint: (e.g., 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```

Fetch changes and check what happens

```
~/git-push-conflict/user2 $ git fetch
~/git-push-conflict/user2 $ git log --oneline --decorate --all --graph
```

```
* 9f95f8c (HEAD -> master) Add .gitignore
| * 6750104 (origin/master) Add README
|/
* 5409b06 (tag: initial) Initial (empty) commit
```

At this point, if `user2` does a `git pull`, the *nasty commit* will appear
```
Merge branch 'master' of /home/user/git-push-conflict/source
```

So, the *nice* thing to do is to rebase our work on top of what's on the server

```
~/git-push-conflict/user2 $ git rebase origin/master
~/git-push-conflict/user2 $ git log --oneline --decorate --all --graph
```
```
* 88ee15a (HEAD -> master) Add .gitignore
* 49db382 (origin/master, origin/HEAD) Update README
* 6750104 Add README
* 5409b06 (tag: initial) Initial (empty) commit
```
Much nicer now...
